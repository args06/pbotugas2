package Level2;

import Level1.Lingkaran;

public class Bola extends Lingkaran {
    private double volume;
    private double luasPermukaan;

    public Bola(double dJariJari) {
        super(dJariJari);
    }

    public double hitungVolume(){
        volume = (4 * super.Luas() * super.getdJariJari())/3;
        return volume;
    }

    public double hitungLuasPermukaan(){
        luasPermukaan = 4 * super.Luas();
        return luasPermukaan;
    }

    public void hitung(){
        hitungVolume();
        hitungLuasPermukaan();
    }

    public double getVolume(){
        return this.volume;
    }

    public double getLuasPermukaan(){
        return this.luasPermukaan;
    }
}
