package SolarSystem;

import java.awt.*;

public class TataSurya {
    Color color;
    private int mass;
    private int diameter;
    private double xLoc;
    private double yLoc;
    private double velX;
    private double velY;
    private double laju;
    private double dirX;
    private double dirY;
    private double jarak;
    boolean visible;
    int[][] orbitDots = new int[1000][2];
    int counter = 0;

    /**
     * Constructor for objects of class Planet
     */
    public TataSurya(double x, double y, double xVelocity, double yVelocity, int bodyMass, int bodyDiameter, Color bodyColor) {
        xLoc = x;
        yLoc = y;
        velX = xVelocity;
        velY = yVelocity;
        mass = bodyMass;
        diameter = bodyDiameter;
        color = bodyColor;
    }

    public double getXPosition() {
        return xLoc;
    }

    public double getYPosition() {
        return yLoc;
    }

    public int getMass() {
        return mass;
    }

    public int getDiameter() {
        return diameter;
    }

    public boolean getDescVisible() {
        return visible;
    }

    public void setDescVisible(boolean b) {
        visible = b;
    }

    public void move() {
        xLoc += velX;
        yLoc += velY;
    }

    public boolean hitPlanet(int x, int y, double scale) {
        return (x > 600 + (getXPosition() - diameter - 600) * scale && x < 600 + (getXPosition() + diameter - 600) * scale &&
                y > 400 + (getYPosition() - diameter - 400) * scale && y < 400 + (getYPosition() + diameter - 400) * scale);
    }

    //update nilai dari x,y, xVel, yVel
    public void update(double StarX, double StarY, int StarMass) {
        if (visible) {
            orbitDots[counter][0] = (int) (xLoc + .5);
            orbitDots[counter][1] = (int) (yLoc + .5);
            counter = (counter + 1) % 1000;
        } else {
            orbitDots = new int[1000][2];
            counter = 0;
        }
        jarak = Math.sqrt(Math.pow((StarX - xLoc), 2) + Math.pow((StarY - yLoc), 2));
        laju = StarMass / Math.pow(jarak, 2);

        dirX = (StarX - xLoc) / jarak;
        dirY = (StarY - yLoc) / jarak;

        velX += dirX * laju;
        velY += dirY * laju;
        move();

    }

    //gambar planet
    public void draw(Graphics g, double size) {
        g.setColor(color);
        g.fillOval((int) (650 + (xLoc - diameter / 2 - 650) * size), (int) (500 + (yLoc - diameter / 2 - 500) * size),
                (int) (diameter * size), (int) (diameter * size));
    }

    //deskripsi planet
    public void dispDesc(Graphics g, double scale) {
        g.setColor(color);
        for (int[] orbit : orbitDots)
            g.drawLine(orbit[0], orbit[1], orbit[0], orbit[1]);
        g.setFont(new Font("Arial", Font.PLAIN, 10));
        g.setColor(Color.MAGENTA);

        g.drawString((Math.round(jarak * 100.0) / 100.0) * 1000000 + " km",
                diameter + (int) (600 + (xLoc - diameter / 2 - 600) * scale), 16 + (int) (400 + (yLoc - diameter / 2 - 400) * scale) + diameter);

    }
}
