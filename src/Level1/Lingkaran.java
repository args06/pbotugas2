package Level1;

public class Lingkaran {
    private double dJariJari;
    protected static double dLuas;
    protected double dKeliling;

    public Lingkaran(double dJariJari) {
        this.dJariJari = dJariJari;
    }

    public double Luas (){
        dLuas = Math.PI * Math.pow(getdJariJari(),2);
        return dLuas;
    }

    public double Keliling (){
        dKeliling = 2 * Math.PI * getdJariJari();
        return dKeliling;
    }

    public void HitungLingkaran(){
        Luas();
        Keliling();
    }

    public double getdJariJari() {
        return dJariJari;
    }

    public double getdLuas() {
        return dLuas;
    }

    public double getdKeliling() {
        return dKeliling;
    }

}
