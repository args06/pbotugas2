import SolarSystem.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.RandomAccessFile;
import java.util.Scanner;
import java.util.StringTokenizer;

public class SolarSystemMain extends JPanel {
    Model model;
    TataSurya[] tataSurya = new TataSurya[9];
    boolean[] deskripsiT = new boolean[9];

    final static int DELAY = 20;
    double size = 1;
    BufferedImage[] bimgs = new BufferedImage[9];
    String[][] deskripsi;
    boolean stop = false;
    int clicked = -1;

    static Matahari matahari;
    static Merkurius merkurius;
    static Venus venus;
    static Bumi bumi;
    static Mars mars;
    static Jupiter jupiter;
    static Saturnus saturnus;
    static Uranus uranus;
    static Neptunus neptunus;

    ImageIcon gambar = new ImageIcon("src/Gambar/volume.jpeg");
    JLabel jGambar =new JLabel(gambar);

    public SolarSystemMain() {
        model = new Model();
        model.setPreferredSize(new Dimension(1200, 800));
        add(model);

        tataSurya[0] = new TataSurya(600, 450, -4.7, 0, 1000, 8, Color.GRAY); //Merkurius
        tataSurya[1] = new TataSurya(752, 400, 0, 2.5, 1000, 12, new Color(207, 153, 52)); //Venus
        tataSurya[2] = new TataSurya(600, 150, 1.8, 0, 1000, 11, Color.BLUE); //Bumi
        tataSurya[3] = new TataSurya(650, -50, 1.2, 0, 1000, 7, Color.RED); //Mars
        tataSurya[4] = new TataSurya(600, -100, 1.2, 0, 1000, 20, new Color(255, 140, 0)); //Jupiter
        tataSurya[5] = new TataSurya(600, -150, 1.2, 0, 1000, 15, new Color(112, 128, 144)); //Saturnus
        tataSurya[6] = new TataSurya(600, -175, 1.2, 0, 1000, 15, new Color(196, 233, 238)); //Uranus
        tataSurya[7] = new TataSurya(0, 400, 0, -1.2, 1000, 13, new Color(66, 98, 243));//Neptunus
        tataSurya[8] = new TataSurya(600, 400, .1, 0, 1000, 30, Color.ORANGE);//Matahari

        setBackground(Color.BLACK);

        deskripsi = new String[][]{
                {"Merkurius",
                        "Jari - jari : " + merkurius.getdJariJari() + "Kilometer",
                        "Panjang Lintasan Revolusi : " + merkurius.getOrbit() + "Juta Km",
                        "Kala Rotasi : " + merkurius.getkRotasi() + "Jam",
                        "Kala Revolusi : " + merkurius.getkRevolusi() + "Hari"
                },
                {"Venus",
                        "Jari - jari : " + venus.getdJariJari() + "Kilometer",
                        "Panjang Lintasan Revolusi : " + venus.getOrbit() + "Juta Km",
                        "Kala Rotasi : " + venus.getkRotasi() + "Jam",
                        "Kala Revolusi : " + venus.getkRevolusi() + "Hari"
                },
                {"Bumi",
                        "Jari - jari : " + bumi.getdJariJari() + "Kilometer",
                        "Panjang Lintasan Revolusi : " + bumi.getOrbit() + "Juta Km",
                        "Kala Rotasi : " + bumi.getkRotasi() + "Jam",
                        "Kala Revolusi : " + bumi.getkRevolusi() + "Hari"
                },
                {"Mars",
                        "Jari - jari : " + mars.getdJariJari() + "Kilometer",
                        "Panjang Lintasan Revolusi : " + mars.getOrbit() + "Juta Km",
                        "Kala Rotasi : " + mars.getkRotasi() + "Jam",
                        "Kala Revolusi : " + mars.getkRevolusi() + "Hari"
                },
                {"Jupiter",
                        "Jari - jari : " + jupiter.getdJariJari() + "Kilometer",
                        "Panjang Lintasan Revolusi : " + jupiter.getOrbit() + "Juta Km",
                        "Kala Rotasi : " + jupiter.getkRotasi() + "Jam",
                        "Kala Revolusi : " + jupiter.getkRevolusi() + "Hari"
                },
                {"Saturnus",
                        "Jari - jari : " + saturnus.getdJariJari() + "Kilometer",
                        "Panjang Lintasan Revolusi : " + saturnus.getOrbit() + "Juta Km",
                        "Kala Rotasi : " + saturnus.getkRotasi() + "Jam",
                        "Kala Revolusi : " + saturnus.getkRevolusi() + "Hari"
                },
                {"Uranus",
                        "Jari - jari : " + uranus.getdJariJari() + "Kilometer",
                        "Panjang Lintasan Revolusi : " + uranus.getOrbit() + "Juta Km",
                        "Kala Rotasi : " + uranus.getkRotasi() + "Jam",
                        "Kala Revolusi : " + uranus.getkRevolusi() + "Hari"
                },
                {"Neptunus",
                        "Jari - jari : " + neptunus.getdJariJari() + "Kilometer",
                        "Panjang Lintasan Revolusi : " + neptunus.getOrbit() + "Juta Km",
                        "Kala Rotasi : " + neptunus.getkRotasi() + "Jam",
                        "Kala Revolusi : " + neptunus.getkRevolusi() + "Hari"
                },
                {"Matahari",
                        "Jari - jari : " + matahari.getdJariJari() + "Kilometer",
                        "Kala Rotasi : " + matahari.getkRotasi() + "Jam"
                },
        };

        bimgs[0] = getImage("src/Gambar/mercury.jpg");
        bimgs[1] = getImage("src/Gambar/Venus.jpg");
        bimgs[2] = getImage("src/Gambar/bluemarble.jpg");
        bimgs[3] = getImage("src/Gambar/mars.jpg");
        bimgs[4] = getImage("src/Gambar/jupiterNasa.jpg");
        bimgs[5] = getImage("src/Gambar/saturn.jpg");
        bimgs[6] = getImage("src/Gambar/uranus.jpg");
        bimgs[7] = getImage("src/Gambar/neptune.jpg");
        bimgs[8] = getImage("src/Gambar/sun.jpg");

        Thread thread = new Thread() {
            @Override
            public void run() {
                rePainting();
            }
        };

        thread.start();
    }

    public static BufferedImage getImage(String ref) {  //load gambar
        BufferedImage bimg = null;
        try {
            bimg = ImageIO.read(new File(ref));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bimg;
    }

    private void rePainting() {
        while (true) {
            if (!stop) {
                for (int i = 0; i < tataSurya.length - 1; i++) {
                    tataSurya[i].update(tataSurya[8].getXPosition(), tataSurya[8].getYPosition(), tataSurya[8].getMass());
                }
            }
            repaint();
            try {
                Thread.sleep(DELAY);
            } catch (InterruptedException ex) {
            }
        }
    }

    class Model extends JPanel implements KeyListener, MouseListener {
        public Model() {
            setFocusable(true);//biar bisa kasih action zoom/pause/exit/klick planet
            requestFocus();
            addKeyListener(this);
            addMouseListener(this);
        }

        public void paintComponent(Graphics g) {
            for (TataSurya body : tataSurya)
                body.draw(g, size);

            for (int i = 0; i < tataSurya.length; i++) {
                if (tataSurya[i].getDescVisible())
                    tataSurya[i].dispDesc(g, size);
            }

            if (clicked > -1) {
                g.drawImage(bimgs[clicked], 0, 0, 200, 200, Color.WHITE, null);
                g.setFont(new Font("Arial", Font.PLAIN, 20));
                g.setColor(Color.WHITE);
                for (int i = 0; i < deskripsi[clicked].length; i++) {
                    g.drawString(deskripsi[clicked][i], 0, 210 + i * 30);
                }
            }

            String zoomIn = "+ KEY  =  Zoom In";
            String zoomOut = "- KEY  =  Zoom Out";
            String spaceBar = "SPACEBAR = Pause/Play";
            String mouseClick = "MOUSECLICK = More Info on Planet";
            String planetNumberInfo = "~The Number beside planet is its";
            String planetNumberInfoSecLine = " distance from the Sun";
            String escape = "ESC = Ilustrasi Volume";

            g.setFont(new Font("Arial", Font.PLAIN, 10));
            g.setColor(Color.WHITE);
            g.drawString(zoomIn, 1000, 50);
            g.drawString(zoomOut, 1000, 70);
            g.drawString(spaceBar, 1000, 90);
            g.drawString(mouseClick, 1000, 110);
            g.drawString(planetNumberInfo, 1000, 130);
            g.drawString(planetNumberInfoSecLine, 1000, 150);
            g.drawString(escape, 1000, 170);

            tataSurya[0].dispDesc(g, size);
            tataSurya[1].dispDesc(g, size);
            tataSurya[2].dispDesc(g, size);
            tataSurya[3].dispDesc(g, size);
            tataSurya[4].dispDesc(g, size);
            tataSurya[5].dispDesc(g, size);
            tataSurya[6].dispDesc(g, size);
            tataSurya[7].dispDesc(g, size);
            tataSurya[8].dispDesc(g, size);
        }

        public void keyTyped(KeyEvent e) {
        }

        public void mousePressed(MouseEvent e) {
        }

        public void mouseReleased(MouseEvent e) {
            for (int i = 0; i < tataSurya.length; i++)
                if (tataSurya[i].hitPlanet(e.getX(), e.getY(), size)) {
                    tataSurya[i].setDescVisible(!tataSurya[i].getDescVisible());
                    if (tataSurya[i].getDescVisible()) {
                        clicked = i;
                    } else {
                        clicked = -1;
                    }
                }
        }

        public void mouseEntered(MouseEvent e) {
        }

        public void mouseExited(MouseEvent e) {
        }

        public void mouseClicked(MouseEvent e) {
        }

        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {

            //aksi zoom in menggunakan +
            if (e.getKeyCode() == KeyEvent.VK_PLUS || e.getKeyCode() == KeyEvent.VK_EQUALS)
                size += .1;

            //aksi zoom out menggunakan -
            if (e.getKeyCode() == KeyEvent.VK_MINUS && size > 0)
                size -= .1;

            //pause jalannya illustrasi
            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                stop = !stop;
            }

            //tambah illustrasi volume
            //Note : 2 kali pencet esc saat pertama kali
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                add(jGambar);
                jGambar.setBounds(0,0,1200,800);
                if (!jGambar.isVisible()){
                    jGambar.setVisible(true);
                } else {
                    jGambar.setVisible(false);
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        double jariJari=0, orbit = 0, kRevolusi = 0, kRotasi=0;
        String[] namaPlanet = {"Matahari", "Merkurius", "Venus", "Bumi", "Mars", "Jupiter", "Saturnus", "Uranus", "Neptunus"};

        try {
            System.out.println("Pilih Jenis input :");
            System.out.println("1. Input Mandiri");
            System.out.println("2. Ambil dari DataPlanet.dat");
            System.out.print("Pilih : ");

            char pilih = scn.next().charAt(0);

            switch (pilih){
                case '1':
                    File database = new File("DataPlanet.dat");
                    database.delete(); // UNTUK DELETE DataPlanet Sebelumnya

                    RandomAccessFile reader1 = new RandomAccessFile("DataPlanet.dat","rw");

                    for (String planet : namaPlanet) {
                        System.out.println(planet);
                        System.out.print("Jari - Jari : ");
                        jariJari = scn.nextDouble();


                        if (!planet.equals("Matahari")) {
                            System.out.print("Panjang Lintasan Orbit : ");
                            orbit = scn.nextDouble();
                            System.out.print("Kala Revolusi : ");
                            kRevolusi = scn.nextDouble();
                        }
                        System.out.print("Kala Rotasi : ");
                        kRotasi = scn.nextDouble();
                        System.out.println();

                        // menyatukan data ke dalam string
                        String data = jariJari + "," + orbit + "," + kRotasi + "," + kRevolusi;

                        // menulis data ke dalam DataPlanet Baru
                        reader1.writeBytes(data + "\n");

                        planetInitial(planet,jariJari,orbit,kRotasi,kRevolusi);
                    }
                    break;

                case '2':
                    RandomAccessFile reader = new RandomAccessFile("DataPlanet.dat","r");
                    //BufferedReader bufferedReader = new BufferedReader(reader);
                    reader.seek(0); // mulai baca dari awal
                    String data = reader.readLine();

                    for (String planet : namaPlanet) {
                        if (data!=null){
                            StringTokenizer tokenizer = new StringTokenizer(data,",");

                            jariJari = Double.parseDouble(tokenizer.nextToken());
                            orbit = Double.parseDouble(tokenizer.nextToken());
                            kRotasi = Double.parseDouble(tokenizer.nextToken());
                            kRevolusi = Double.parseDouble(tokenizer.nextToken());
                            //System.out.println(tokenizer.nextToken());

                            System.out.println(planet);
                            System.out.println("Jari - Jari : " + jariJari + " km");

                            if (!planet.equals("Matahari")) {
                                System.out.println("Panjang Lintasan Orbit : " + orbit + " km");
                                System.out.println("Kala Revolusi : " + kRevolusi + " jam");
                            }

                            System.out.println("Kala Rotasi : " + kRotasi + " jam\n");

                            data = reader.readLine();

                            planetInitial(planet,jariJari,orbit,kRotasi,kRevolusi);

                        }
                    }
                    reader.close();
                    break;

                default:
                    System.err.println("Tidak ada opsi");
            }


        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new JFrame("Solar System Illustration");
                frame.setContentPane(new SolarSystemMain());
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
        });
    }

    public static void planetInitial(String planet, double jariJari, double orbit, double kRotasi, double kRevolusi){
        switch (planet) {
            case "Matahari":
                matahari = new Matahari(jariJari, orbit, kRotasi, kRevolusi);
                break;
            case "Merkurius":
                merkurius = new Merkurius(jariJari, orbit, kRotasi, kRevolusi);
                break;
            case "Venus":
                venus = new Venus(jariJari, orbit, kRotasi, kRevolusi);
                break;
            case "Bumi":
                bumi = new Bumi(jariJari, orbit, kRotasi, kRevolusi);
                break;
            case "Mars":
                mars = new Mars(jariJari, orbit, kRotasi, kRevolusi);
                break;
            case "Jupiter":
                jupiter = new Jupiter(jariJari, orbit, kRotasi, kRevolusi);
                break;
            case "Saturnus":
                saturnus = new Saturnus(jariJari, orbit, kRotasi, kRevolusi);
                break;
            case "Uranus":
                uranus = new Uranus(jariJari, orbit, kRotasi, kRevolusi);
                break;
            case "Neptunus":
                neptunus = new Neptunus(jariJari, orbit, kRotasi, kRevolusi);
                break;
        }
    }
}