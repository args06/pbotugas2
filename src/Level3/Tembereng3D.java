package Level3;

import Level2.Kerucut;

public class Tembereng3D extends Kerucut {
    private double tinggiTembereng;
    private double volumeTembereng;
    private double luasPermukaanTembereng;

    public Tembereng3D(double jariJari, double dTinggi, double tinggiTembereng) {
        super(jariJari, dTinggi);
        this.tinggiTembereng = tinggiTembereng;
    }

    private double volumeTemberengProcess() {
        volumeTembereng = ((0.5 * super.Keliling()) - (0.333 * Math.PI * getTinggiTembereng()))
                * Math.pow(getTinggiTembereng(), 2);
        return volumeTembereng;
    }

    private double luasPermukaanTemberengProcess() {
        luasPermukaanTembereng = 2 * Math.PI * Math.pow(super.getdJariJari(), 2);
        return luasPermukaanTembereng;
    }

    public void processAll() {
        volumeTemberengProcess();
        luasPermukaanTemberengProcess();
    }

    public double getVolumeTembereng() {
        return volumeTembereng;
    }

    public double getTinggiTembereng() {
        return tinggiTembereng;
    }

    public double getLuasPermukaanTembereng() {
        return luasPermukaanTembereng;
    }
}